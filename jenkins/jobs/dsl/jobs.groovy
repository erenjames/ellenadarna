def workspaceFolderName = "${WORKSPACE_NAME}"
def projectFolderName = "${PROJECT_NAME}"
def samplefolderName = projectFolderName + "/SampleFolder" 
def samplefolder = folder(samplefolderName) { displayName ('Folder created by Jenkins') }

//Jobs 

def job1 = freeStyleJob(samplefolderName + "/FirstJob")
def job2 = freeStyleJob(samplefolderName + "/SecondJob")

//pipeline
def samplepipeline = buildPipelineView(samplefolderName + "/Sample-Pipeline-Demo")

samplepipeline.with{
title('Pipeline Demo')
displayedBuilds(5)
selectedJob(samplefolderName +"/FirstJob")
showPipelineParameters()
refreshFrequency(5)
}


// Job Configurations 

job1.with{
   scm {
     git {
	   remote { 
	   url ('https://erenjames@bitbucket.org/erenjames/ellenadarna.git')
	   credentials("adop-jenkins-master")
	   }
	   branch("*/master")
	   }
	  }
	steps{
shell ('''#!/bin/sh 
ls -lart''')
}

publishers{
downstreamParameterized{
	trigger(samplefolderName + "/SecondJob"){
	condition("SUCCESS")
	parameters{
	predefinedProp("CUSTOM_WORKSPACE",'$WORKSPACE')
				}
			}
		}
	}

}
job2.with{
	parameters{
		stringParam("CUSTOM_WORKSPACE","","")
		}
	steps{
	shell('''
	#!/bin/sh
	ls -lart $CUSTOM_WORKSPACE''')
	} 
}